create table ufo_sightings (
	id int not null auto_increment,
	datetime datetime(),
	city varchar(255) null,
	state char(2) null,
	country char(2),
	shape varchar(100),
	duration_seconds time(),
	duration_hour_min varchar(100),
	comments text(),
	date_posted date(),
	lat decimal(10,8),
	lng decimal(11,8)
);

insert into ufo_sightings (
	datetime,
	city,
	state,
	country,
	shape,
	duration_seconds,
	duration_hour_min,
	comments,
	date_posted,
	lat,
	lng
) values
('10/10/1949 20:30', 'san marcos', 'tx', 'us', 'cylinder', '2700', '45 minutes', 'This event took place in early fall around 1949-50. It occurred after a Boy Scout meeting in the Baptist Church. The Baptist Church sit', '4/27/2004', '29.8830556', '-97.9411111'),
('10/10/1949 21:00', 'lackland afb', 'tx', null, 'light', '7200', '1-2 hrs', '1949 Lackland AFB&#44 TX.  Lights racing across the sky &amp; making 90 degree turns on a dime.',	'12/16/2005'	'29.38421',	'-98.581082'),
('10/10/1955 17:00', 'chester (uk/england)', null, 'gb', 'circle', '20', '20 seconds', 'Green/Orange circular disc over Chester&#44 England', '1/21/2008', '53.2', '-2.916667'),
('10/10/1956 21:00', 'edna', 'tx', 'us', 'circle', '20', '1/2 hour', 'My older brother and twin sister were leaving the only Edna theater at about 9 PM&#44...we had our bikes and I took a different route home', '1/17/2004', '28.9783333', '-96.6458333'),
('10/10/1960 20:00', 'kaneohe', 'hi', 'us', 'light', '900', '15 minutes', 'AS a Marine 1st Lt. flying an FJ4B fighter/attack aircraft on a solo night exercise&#44 I was at 50&#44000&#39 in a &quot;clean&quot; aircraft (no ordinan', '1/22/2004', '21.4180556', '-157.8036111'),
('10/10/1961 19:00', 'bristol', 'tn', 'us', 'sphere', '300', '5 minutes', 'My father is now 89 my brother 52 the girl with us now 51 myself 49 and the other fellow which worked with my father if he&#39s still livi', '4/27/2007', '36.595', '-82.1888889');
